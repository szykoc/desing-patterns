package builder.inteface;

public class ComputerBuilderImpl implements ComputerBuilder{

    private Computer computer;

    public ComputerBuilderImpl() {
        this.computer = new Computer();
    }

    @Override
    public Computer build() {
        return computer;
    }

    @Override
    public ComputerBuilder setGhz(double ghz) {
        computer.setGhz(ghz);
        return this;
    }

    @Override
    public ComputerBuilder setRam(int ram) {
        computer.setRam(ram);
        return this;
    }

    @Override
    public ComputerBuilder setMonitorSize(double monitorSize) {
        computer.setMonitorSize(monitorSize);
        return this;
    }

    @Override
    public ComputerBuilder setAge(int age) {
        computer.setAge(age);
        return this;
    }

    @Override
    public ComputerBuilder setWeight(double weight) {
        computer.setWeight(weight);
        return this;
    }

    @Override
    public ComputerBuilder setOs(String os) {
        computer.setOs(os);
        return this;
    }
}
