package builder.inteface;

public class Main {
    public static void main(String[] args) {

        Computer computerMilleniumOs = new ComputerBuilderImpl()
                .setAge(1)
                .setGhz(2.4)
                .setMonitorSize(24.0)
                .setOs("Millenium")
                .setRam(4)
                .setWeight(1.5)
                .build();

        System.out.println(computerMilleniumOs);
    }
}
