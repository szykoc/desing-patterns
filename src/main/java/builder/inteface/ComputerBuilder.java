package builder.inteface;

public interface ComputerBuilder {
    Computer build();
    ComputerBuilder setGhz(double ghz);
    ComputerBuilder setRam(int ram);
    ComputerBuilder setMonitorSize(double monitorSize);
    ComputerBuilder setAge(int age);
    ComputerBuilder setWeight(double weight);
    ComputerBuilder setOs(String os);
}
