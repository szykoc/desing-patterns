package builder.inteface;

public class Computer {
    private double ghz;
    private int ram;
    private double monitorSize;
    private int age;
    private double weight;
    private String os;

    public Computer setGhz(double ghz) {
        this.ghz = ghz;
        return this;
    }

    public Computer setRam(int ram) {
        this.ram = ram;
        return this;
    }

    public Computer setMonitorSize(double monitorSize) {
        this.monitorSize = monitorSize;
        return this;
    }

    public Computer setAge(int age) {
        this.age = age;
        return this;
    }

    public Computer setWeight(double weight) {
        this.weight = weight;
        return this;
    }

    public Computer setOs(String os) {
        this.os = os;
        return this;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "ghz=" + ghz +
                ", ram=" + ram +
                ", monitorSize=" + monitorSize +
                ", age=" + age +
                ", weight=" + weight +
                ", os='" + os + '\'' +
                '}';
    }
}
