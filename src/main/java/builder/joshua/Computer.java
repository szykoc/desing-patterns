package builder.joshua;

public class Computer {
    private double ghz;
    private int ram;
    private double monitorSize;
    private int age;
    private double weight;
    private String os;

    public Computer(ComputerBuilder computerBuilder) {
        this.ghz = computerBuilder.ghz;
        this.ram = computerBuilder.ram;
        this.monitorSize = computerBuilder.monitorSize;
        this.age = computerBuilder.age;
        this.weight = computerBuilder.weight;
        this.os = computerBuilder.os;
    }

    public double getGhz() {
        return ghz;
    }

    public int getRam() {
        return ram;
    }

    public double getMonitorSize() {
        return monitorSize;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    public String getOs() {
        return os;
    }


    @Override
    public String toString() {
        return "Computer{" +
                "ghz=" + ghz +
                ", ram=" + ram +
                ", monitorSize=" + monitorSize +
                ", age=" + age +
                ", weight=" + weight +
                ", os='" + os + '\'' +
                '}';
    }

    static class ComputerBuilder{
        private double ghz;
        private int ram;
        private double monitorSize;
        private int age;
        private double weight;
        private String os;

        public ComputerBuilder(int ram) {
            this.ram = ram;
        }

        public ComputerBuilder setGhz(double ghz) {
            this.ghz = ghz;
            return this;
        }

        public ComputerBuilder setMonitorSize(double monitorSize) {
            this.monitorSize = monitorSize;
            return this;
        }

        public ComputerBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        public ComputerBuilder setWeight(double weight) {
            this.weight = weight;
            return this;
        }

        public ComputerBuilder setOs(String os) {
            this.os = os;
            return this;
        }

        public Computer build(){
            return new Computer(this);
        }
    }
}
