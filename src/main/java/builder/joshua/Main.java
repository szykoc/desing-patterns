package builder.joshua;

public class Main {
    public static void main(String[] args) {
        Computer computerWin98 = new Computer.ComputerBuilder(5)

                .build();

        System.out.println(computerWin98);
    }
}
