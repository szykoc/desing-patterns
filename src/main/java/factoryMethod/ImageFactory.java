package factoryMethod;

public class ImageFactory {

    public static Image createImage(String extension){
        switch (extension){
            case "jpg":
                return new JpgImage();
            case "gif":
                return new GifImage();
            default:
                throw new UnsupportedOperationException(
                        "Extension not supported"
                );
        }
    }
}
