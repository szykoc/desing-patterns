package factoryMethod;

public class JpgImage implements Image {
    @Override
    public String getExtension() {
        return "jpg";
    }
}
