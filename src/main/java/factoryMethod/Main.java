package factoryMethod;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj rozszerzenie pliku");
        String extenstion = scanner.next();

        Image image = ImageFactory.createImage(extenstion);

        System.out.println("Stworzyłes pilik o rozszerzeniu " +
                image.getExtension());
    }
}
