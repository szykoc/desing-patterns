package factoryMethod;

public interface Image {
    String getExtension();
}
