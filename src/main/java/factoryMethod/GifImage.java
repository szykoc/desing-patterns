package factoryMethod;

public class GifImage implements Image {
    @Override
    public String getExtension() {
        return "gif";
    }
}
