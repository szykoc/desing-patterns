package abstractFactory;

public class ConcreteFactoryProducer {

    public static ButtonsFactory getFactory(String color){
        switch (color){
            case "WHITE":
                return new WhiteFactory();
            case "DARK":
                return new DarkFactory();
            default:
                throw new UnsupportedOperationException("No factory");
        }
    }

}
