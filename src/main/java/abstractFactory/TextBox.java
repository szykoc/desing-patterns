package abstractFactory;

public class TextBox extends Component {

    public TextBox(Color color) {
        super(color);
    }

    @Override
    public String getDescription() {
        return "Tworze textbox koloru " + getColor();
    }
}
