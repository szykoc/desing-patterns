package abstractFactory;

public class Label extends Component {

    public Label(Color color) {
        super(color);
    }

    @Override
    public String getDescription() {
        return "Tworze label koloru:  " + getColor();
    }
}
