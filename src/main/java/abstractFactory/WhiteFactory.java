package abstractFactory;

public class WhiteFactory implements ButtonsFactory{

    @Override
    public Label getLabel() {
        return new Label(Component.Color.WHITE);
    }

    @Override
    public TextBox getTextBox() {
        return new TextBox(Component.Color.WHITE);
    }
}
