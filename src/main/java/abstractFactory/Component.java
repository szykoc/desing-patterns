package abstractFactory;

public abstract class Component {

    private Color color;

    public Component(Color color) {
        this.color = color;
    }

    public enum Color {
        DARK, WHITE
    }

    public Color getColor() {
        return color;
    }

    public abstract String getDescription();
}
