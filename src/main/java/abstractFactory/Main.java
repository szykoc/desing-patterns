package abstractFactory;

public class Main {
    public static void main(String[] args) {

        // na zadanie przerobić ta factory na factory samochodow bmw, audi , mercedes produkujaca czesci samochodow

        ButtonsFactory white = ConcreteFactoryProducer.getFactory("WHITE");
        ButtonsFactory darkFactory = ConcreteFactoryProducer.getFactory("DARK");

        System.out.println(white.getLabel().getDescription());
        System.out.println(darkFactory.getLabel().getDescription());

    }
}
