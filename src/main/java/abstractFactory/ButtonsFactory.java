package abstractFactory;

import org.w3c.dom.Text;

public interface ButtonsFactory {
    Label getLabel();
    TextBox getTextBox();
}
