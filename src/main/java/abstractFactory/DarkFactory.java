package abstractFactory;

public class DarkFactory implements ButtonsFactory {

    @Override
    public Label getLabel() {
        return new Label(Component.Color.DARK);
    }

    @Override
    public TextBox getTextBox() {
        return new TextBox(Component.Color.DARK);
    }
}
