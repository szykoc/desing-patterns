package strategy;

public class PayPalStrategy implements PaymentStrategy{

    @Override
    public String paymentMethod() {
        return "Wybrałes opcje paypal";
    }
}
