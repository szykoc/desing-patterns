package strategy;

public class CashStrategy implements PaymentStrategy {
    @Override
    public String paymentMethod() {
        return "Wybrałes opcje cash";
    }
}
