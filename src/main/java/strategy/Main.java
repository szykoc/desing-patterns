package strategy;

public class Main {
    public static void main(String[] args) {
        PaymentStrategy cashStrategy = new CashStrategy();

        OnlineShop onlineShop = new OnlineShop(cashStrategy);

        System.out.println(onlineShop.payMethod());

        PaymentStrategy payPalStrategy = new PayPalStrategy();

        onlineShop.setPaymentStrategy(payPalStrategy);

        System.out.println(onlineShop.payMethod());
    }
}
