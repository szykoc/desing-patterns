package strategy;

public class OnlineShop {
    private PaymentStrategy paymentStrategy;

    public OnlineShop(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public String payMethod(){
        return paymentStrategy.paymentMethod();
    }

    public void setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }
}
