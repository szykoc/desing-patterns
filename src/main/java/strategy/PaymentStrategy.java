package strategy;

public interface PaymentStrategy {
    String paymentMethod();
}
