package decorator;

public abstract class Beverage {

    public String description;

    public abstract double calculatePrice();
    public abstract String getDescription();
}
