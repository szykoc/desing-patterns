package decorator;

public class SugarBeverageDecorator extends AddonDecorator {

    protected SugarBeverageDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double calculatePrice() {
        double priceWithoutDecoration = super.calculatePrice();
        return priceWithoutDecoration + 1.0;
    }

    @Override
    public String getDescription() {
        String descriptionWithoutDecorator = super.getDescription();
        return descriptionWithoutDecorator + " with Sugar za zeta" ;
    }
}
