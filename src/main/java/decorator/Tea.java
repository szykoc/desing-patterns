package decorator;

public class Tea extends Beverage {

    @Override
    public double calculatePrice() {
        return 5.0;
    }

    @Override
    public String getDescription() {
        return "Tea za piataka";
    }
}
