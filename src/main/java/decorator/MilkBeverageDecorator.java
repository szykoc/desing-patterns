package decorator;

public class MilkBeverageDecorator extends AddonDecorator {

    private static final double MILK_PRICE = 2.0;

    protected MilkBeverageDecorator(Beverage beverage) {
        super(beverage);
    }

    @Override
    public double calculatePrice() {
        double priceWithoutDecorating = super.calculatePrice();
        return priceWithoutDecorating + MILK_PRICE;
    }

    @Override
    public String getDescription() {
        String descriptionWithoutDecorating = super.getDescription();
        return descriptionWithoutDecorating + " with Milk za " + MILK_PRICE;
    }
}
