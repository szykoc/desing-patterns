package decorator;

public abstract class AddonDecorator extends Beverage {

    private Beverage beverage;

    protected AddonDecorator(Beverage beverage) {
        this.beverage = beverage;
    }

    @Override
    public double calculatePrice() {
        return beverage.calculatePrice();
    }

    @Override
    public String getDescription() {
        return beverage.getDescription();
    }
}
