package decorator;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;

public class Main {

    public static void main(String[] args) {
        Beverage coffeWithMilk =
                new MilkBeverageDecorator(
                        new Coffe()
                );

        System.out.println(coffeWithMilk.calculatePrice());
        System.out.println(coffeWithMilk.getDescription());

        Beverage teaWithMilkAndSugar =
                new MilkBeverageDecorator(
                    new SugarBeverageDecorator(
                        new Tea()
                )
            );

        System.out.println(teaWithMilkAndSugar.calculatePrice());
        System.out.println(teaWithMilkAndSugar.getDescription());

    }
}
