package decorator;

public class Coffe extends Beverage {

    @Override
    public double calculatePrice() {
        return 5.5;
    }

    @Override
    public String getDescription() {
        return "Coffe za 5.5";
    }
}
